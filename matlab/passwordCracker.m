function passlist = passwordCracker(inputtxt)
lengthtxt = length(inputtxt);            % Get length of input string.
randomindex = randperm(lengthtxt);       % Generate indices from lengthtxt.
passlist = perms(inputtxt(randomindex)); % Get all permutations of a
                                         % permutation of the inputtxt.
end