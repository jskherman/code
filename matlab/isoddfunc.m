function isoddfunc(num);          % start function definition and assign
                                  % variable num for user's input of an
                                  % integer as an argument
odd = logical(mod(num,2))         % assigns a variable "odd" for the
                                  % converted output to logical of the
                                  % modulus function whose divisor is 2 and
                                  % the dividend is the number (num)
                                  % supplied by the user 
end                               % ends the function