function [sum,prod] = sumprod(x,y)
sum = x+y;
prod = x*y;
end