\chapter{Debating the Motion}

	Every member has the right to debate or to discuss business that is introduced to the assembly in the form of a main motion. Only a motion to limit debate or to close debate (which is accomplished through a motion called \emph{previous question}) can take away or limit this right, and either motion must have a two-thirds vote.
	
	The assembly can make an informed decision from the facts and persuasive arguments that members present only through discussion. Members should never be tempted to \emph{gavel through} an issue (rush through a motion without any discussion) in an effort to save time or silence the opposition.
	
	\section{Rules of Debate}
	
	Even though members have the right to debate, established parliamentary rules concerning the privileges of debate exist:
	
	\begin{itemize}
		\item A member must obtain the floor and be recognized by the presiding officer before beginning to speak. A member can’t begin talking while seated. However, in small board meetings where rules of debate are less formal, talking while seated is allowed.
		\item The member who made the motion has the first right to speak to the motion. He or she does so by rising and obtaining the floor after the chair places the motion before the assembly for discussion.
		\item A member can speak twice to the motion on the same day, but he or she can take the second turn only after everyone who wishes to speak the first time has spoken. If debate on the motion is continued at the next meeting, which is held on another day, the member’s right to debate is renewed.
		\item Each member can speak for ten minutes on each turn unless the assembly has adopted rules that state another amount of time.
		\item Debate must be germane (relevant) to the motion.
		\item Speakers must address all remarks to the chair; cross talk between members is not allowed.
		\item Speakers must be courteous and never attack other members or question the motives of the members. In controversial issues, the discussion is focused on ideas, not on personalities. Members must not use inflammatory statements such as “it’s a lie,” “it is a fraud,” or “he’s a liar.” Profane language is also prohibited.
		\item In debate, speakers refer to officers by title and avoid mentioning other members’ names. Instead, they should refer to the members by identifiers such as “the member who just spoke” or “the delegate from Hawaii.”
		\item When speaking to a motion, it is important for the member to first let the assembly know which side of the issue he or she is on and gives the reasons why. Doing so helps the chair alternate the debate.
		\item The member who makes the motion can’t speak against his or her own motion, although he or she can vote against it. The person who seconds the motion, however, can speak against the motion because a second means “Let’s discuss it,” not “I agree.” Sometimes a member seconds a motion so he or she can speak against it.
		\item A member can’t read (or have the secretary read) from part of a manuscript or book as part of his or her debate without the permission of the assembly. However, the member can read short, relevant printed extracts in debate to make a point.
		\item During debate, a member can’t talk against a previous action that is not pending, unless one of the motions to \emph{rescind, reconsider,} or \emph{amend something previously adopted} is pending; or unless the member concludes his or her remarks with one of these motions.
		\item During debate, members should take care not to disturb the assembly by whispering, talking, walking across the floor, or causing other distractions.
		\item During debate, the presiding officer sits down when a member is assigned the floor to speak. Or if, when seated, members can’t see the presiding officer, the officer stands back from the lectern while the member is speaking. (Like the rule of one item of business at a time, this rule allows only one person at a time to have the floor.)
		\item If at any time during debate the presiding officer needs to interrupt the speaker for a ruling (for example, if the chair is correcting something that the speaker is doing) or needs to give information (facts related to the discussion, for example), the member should sit down until the presiding officer finishes. The member can then resume speaking.
		\item In deliberative assemblies (bodies that meet to consider proposals made to them), members do not have the right to give some of their time to another member. If a member has not used his or her ten minutes, the member forfeits the unused portion.
		\item As the chairman, the presiding officer must remain impartial. As a member, the presiding officer has a right to debate. Thus, if the presiding officer wishes to speak to an issue, he or she relinquishes the chair to another officer (the vice president, for example) who has not spoken and does not wish to speak. If no officer wishes to take the chair, a member who has not spoken and has received the assembly’s approval can preside. The presiding officer resumes the chair when the motion has been either voted on by the assembly or temporarily put aside by a motion to \emph{refer to a committee, postpone to another time,} or \emph{lay on the table}.
		\item In debating an issue, members also have the right to conclude their debate with a higher-ranking motion than the one pending. This action upholds the parliamentary principle that when the chair recognizes a member for any legitimate purpose, the member has the floor for all legitimate purposes.
	\end{itemize}

	\section{Limitations on Debate}
	
	Members can put limits on debate and even stop the debate altogether. To do so, members must make a motion. The presiding officer cannot cut off the debate as long as one member wishes to rise and speak. Neither can one member stop the debate by yelling out “Question” or “It’s time to take a vote.”
	
	Only the motion to \emph{limit debate} can limit debate; and debate can be closed only by the motion previous question or close debate. These motions need a second, are not debatable, and require a two-thirds vote to adopt. A rising (but not counted) vote is required.
	
	\section{Debatable and Undebatable Motions}
	
	Not all motions are debatable. Some motions are debatable in some situations and not in others. It is important to study the chapters on motions to understand what each motion is and the reasons why some motions are debatable and some aren’t.
	
	Debatable:
	\begin{itemize}
		\item Main motion
		\item Postpone indefinitely
		\item Amend
		\item Refer to a committee
		\item Postpone to a certain time
		\item Appeal from the decision of the chair \item Rescind
		\item Amend something previously adopted \item Reconsider
		\item Recess (as an incidental main motion)
		\item Fix the time to which to adjourn (as an incidental main motion)
	\end{itemize}

	Undebatable:
	\begin{itemize}
		\item Limit or extend the limits of debate \item Previous question (close debate)
		\item Lay on the table
		\item Take from the table
		\item Call for the orders of the day
		\item Raise a question of privilege
		\item Recess (as a privileged motion)
		\item Adjourn
		\item Fix the time to which to adjourn (as a privileged motion)
		\item Point of order
		\item Withdraw a motion
		\item Suspend the rules
		\item Object to consideration of the motion \item Division of the assembly
		\item Division of the question
		\item Incidental motions relating to voting, when the subject is pending
		\item Dispense with the reading of the minutes
	\end{itemize}